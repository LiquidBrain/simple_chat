const express = require('express');
const server = express();
const http = require('http').Server(server);
const io = require('socket.io')(http);
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const IOcookieParser = require('socket.io-cookie-parser');

server.use(express.static(__dirname + '/public'));
server.use(bodyParser.urlencoded({extended: false}));
server.use(bodyParser.json());
server.use(cookieParser());
server.use(session({
    secret: 'wertyuio',
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 60000}
}));

io.use(IOcookieParser());

require('./src/routes')(server);
require('./src/socket/handler')(io);

http.listen(3000, function () {
    console.log('listening on *:3000');
});

