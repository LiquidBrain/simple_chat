function getActiveUsers(users) {
    let result = [];

    for (let i in users) {
        let user = users[i];

        if (user.isActive) {
            result.push(user);
        }
    }

    return result;
}

module.exports = function (io) {

    //List oal all active users
    const users = {};

    //List of last messages (30m)
    const messages = [];

    //Socket handler
    io.on('connection', function (socket) {
        let username = socket.request.cookies.username;

        //Add user to users list
        users[username] = {
            username: username,
            isActive: true,
            lastActive: new Date
        };

        //Send active users
        io.emit('users', getActiveUsers(users));

        //Send last messages
        socket.emit('messages', messages);

        //Send "User join to chat"
        socket.broadcast.emit('join', {username: username});

        socket.on('disconnect', function () {
            delete users[username];
            //Send active users
            io.emit('users', getActiveUsers(users));

            //Send "User leave from chat"
            socket.broadcast.emit('leave', {username: username});
        });

        socket.on('message', function (message) {
            //Update last user activity
            users[username].lastActive = new Date;
            users[username].isActive = true;

            const msg = {
                username: username,
                message: message,
                time: new Date
            };

            //Send to all users
            io.emit('message', msg);

            //Add to messages
            messages.push(msg);
        });
    });

    //Run messages cleaner
    setInterval(() => {
        console.log('Start messages cleaner');

        let now = new Date();

        //Delete emails older than 1 minute
        messages.forEach(message => {
            if ((now - message.time) / 1000 > 60) {
                const index = messages.indexOf(message);

                if (index !== -1) {
                    console.log('Message expired:', message);
                    messages.splice(index, 1);
                }
            }
        });
    }, 5000);

    //Run active users watcher
    setInterval(() => {
        let now = new Date();
        let inactiveUsers = 0;

        for (let i in users) {
            let user = users[i];

            //Users who have been active for the last 30 seconds
            if ((now - user.lastActive) / 1000 > 30) {
                users[i].isActive = false;
                inactiveUsers++;
            }
        }

        //Send active users
        io.emit('users', getActiveUsers(users));
    }, 5000);
};

