const path = require('path');

module.exports = function (server) {
    server.get('/', function (req, res) {
        const file = req.cookies.username ? 'index.html' : 'login.html';

        res.sendFile(path.join(__dirname + '/../resources/views/' + file));
    });

    server.post('/', function (req, res) {
        res.cookie('username', req.body.username);

        res.redirect('/');
    });
};